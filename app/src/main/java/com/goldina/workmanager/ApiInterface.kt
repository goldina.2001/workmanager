package com.goldina.workmanager

import com.goldina.workwithrestapi.MyData
import com.goldina.workwithrestapi.MyDataItem
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.PUT
import retrofit2.http.Path

interface ApiInterface {
    @GET("posts")
    fun getData():Call<List<MyDataItem>>

    @GET("posts/{id}")
    fun getOnePost(@Path("id") id:Int):Call<MyDataItem>


}