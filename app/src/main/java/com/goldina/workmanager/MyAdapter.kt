package com.goldina.workmanager

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.goldina.workwithrestapi.MyDataItem
import kotlinx.android.synthetic.main.row_items.view.*

class MyAdapter(val context: Context, val userList: List<MyDataItem>)
    :RecyclerView.Adapter<MyAdapter.MyHolder>(){
    class MyHolder(itemView:View):RecyclerView.ViewHolder(itemView) {
        var messageId:TextView
        var title:TextView

        init {
            messageId=itemView.messageId
            title = itemView.title
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        var itemView = LayoutInflater.from(context).inflate(R.layout.row_items,parent,false)
        return MyHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyHolder, position: Int) {
        holder.messageId.text = userList[position].id.toString()
        holder.title.text = userList[position].title.toString()
    }

    override fun getItemCount(): Int {
        return userList.size
    }

}