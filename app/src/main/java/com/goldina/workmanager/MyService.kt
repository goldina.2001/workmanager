package com.goldina.workmanager

import android.annotation.SuppressLint
import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import com.goldina.workwithrestapi.MyData
import com.goldina.workwithrestapi.MyDataItem
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MyService : Service() {

    override fun onBind(intent: Intent): IBinder? = null

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        // Send a notification that service is started
        Log.i("SERVICE", "Service started...")
        val thread = Thread(Runnable {
            while(true) {
                setOneTimeWorkRequest()
                Thread.sleep(3000)
            }
        })
        thread.start()

        return START_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("SERVICE", "Service destroyed...")
    }
    private fun setOneTimeWorkRequest(){

        val uploadRequest = OneTimeWorkRequest
            .Builder(UploadWorker::class.java)
            .build()
        WorkManager.getInstance(applicationContext).enqueue(uploadRequest)

    }

}