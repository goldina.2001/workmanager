package com.goldina.workmanager

import android.content.Context
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.goldina.workwithrestapi.MyDataItem
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

const val BASE_URL="https://jsonplaceholder.typicode.com/"

class UploadWorker(context: Context,params:WorkerParameters):Worker(context,params) {
    override fun doWork(): Result {
        return try {
            val retrofitBuilder = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
                .create(ApiInterface::class.java)
            for (i in 1..100){
                val retrofitData = retrofitBuilder.getOnePost(i)
                retrofitData.enqueue(object : Callback<MyDataItem?> {
                    override fun onResponse(
                        call: Call<MyDataItem?>,
                        response: Response<MyDataItem?>,
                    ) {
                        val data = response.body()!!
                        Log.i("WorkMngr", "id:${data.id}, title:${data.title}")
                    }

                    override fun onFailure(call: Call<MyDataItem?>, t: Throwable) {
                        Log.i("WorkMngr", "onFailure: ${t.message}")
                    }
                })
            }
            Result.success()
        }catch (e:Exception){
            Result.failure()
        }
    }

}